import s from "./style.module.css"
import {useState} from "react";

export function MenuListItem({onClick, difficulty, isSelected}) {
    const [isHovered, setIsHovered] = useState(false)
    function getBackgroundColor() {
        if(isHovered){
            return "#2aa9ee"
        } else if(isSelected) {
            return "#68aff1"
        } else {
            return "#d7ebfd"
        }
    }
    return (
    <div
        onClick={() => onClick(difficulty)}
        style={{
            backgroundColor : getBackgroundColor()
        }}

        className={s.container} onMouseEnter={() => setIsHovered(true)} onMouseLeave={() => setIsHovered(false)}>
        <p>Set to : {difficulty}</p>
    </div>
    )
}